# Word blend acceptability

## Goal

Understanding how well humans accept word blends as a function of phonotactic probability.

## Method

Human labeling of the words in a blend.

## Stimuli

Known and novel word blends. The novel word blends will vary in phonotactic probability.

### Example

Known blends:

    * liger
    * labradoodle

Novel blends:

    * lion + cheetah

        * high: leetah /litx/
        * low: cheeton /Cixn/

    * terrier + poodle

        * high: terridle /tErIdxl/
        * low: poodier /pudiR/
