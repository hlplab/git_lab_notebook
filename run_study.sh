#!/bin/sh
# run the word blend lexical decision study
# Esteban Buz <esteban.buz@gmail.com>

stimuli_file=pilot0.2.tab
list=1

echo "Type in a name to save the data with then press [enter]:"

read datafile

datafile="data/${stimuli_file%.*}/${datafile}.tab"

#make sure path exists for datafile
mkdir -p "${datafile%/*}"
#clear previous data file, this is generally not a good idea
rm -f $datafile

while read -u 10 trial
do
    read li cond word1 word2 blend <<< $trial
    if [[ $li != $list ]]
    then
        continue
    fi
    clear

    echo "\n\n\n\t\t${blend}\n\n\n"

    echo "What two animal names are in this blend? Type in the two names and press [enter]:"

    read resp

    echo "$stimuli_file $trial $resp"  >> $datafile

done 10< "stimuli/${2-$stimuli_file}" #put input to avoid blocking stdin

clear

echo "thanks!"
