# Git repository as a lab notebook

We can use git to create a lab notebook.
This notebook could be where you keep experimental design ideas, stimuli and experimental code, data and analysis code, publications, or any subset of these things.
Here I'll walk through [an example](https://bitbucket.org/hlplab/git_lab_notebook) of using git to keep a lab notebook that lets us keep track of our project and the various ways it will get morphed as it continues.

We will use two features of git and other DVCSs to make this workflow: _tags_ and _branches_.

## Using tags to mark-up progress

In git you can _tag_ a commit with an identifier.
Tags come in two flavors, lightweight tags and annotated tags.
Annotated tag can have their own message and other identifying information that a commit has (like an author). We will focus on annotated tags here.

A tag can be thought of as, simultaneously, an alias for a commit and an annotation of that commit.
Let's say you are working on some experimental code and you have been making incremental updates to it.
After some coding you feel you're ready to pilot so you `git commit -m 'code is ready to pilot!'`.
We can visualize your repository as this (`a <-b` means `a` is an ancestor of `b`, i.e. we commited changes to `a` and ended up at `b`):

    c1 <-c2 <-c3 <-c4

### Tagging a commit

Being a meticulous person, you mark the commit that has all the files to run your pilot.
You can include this information in the commit message but tags let you do this in a more human-frieldnly way.
Running `git log` will show you a series of entries, each of which corresponds to a commit in your repository.
Each commit has a unique identifying _checksum_ and these can be use to reference commits.
Checksums are a long string of numbers and letters that are meaningful to git but not particularly meaningful nor easy to remember for us.
Further, we might have forgotten to mention that we'd be running a pilot in our commit message so looking through our logs will not help us find which state of our project corresponds to which pilot.

To solve both these problems we tag the commit as being the pilot version using:

    git tag -a pilot0.1 -m 'pilot version 1'

You can tag a commit after the fact by referencing its checksum in the tag command: 

    git tag -a pilot0.1 xxxxx -m 'pilot version 1'

So we can visualize our commits and tags in the repository this way:

    c1 <-c2 <-c3 <-c4
                   ^
                   pilot0.1

You continue your coding and start working on the second pilot and make a few more commits and tag where the second pilot is:

    c1 <-c2 <-c3 <-c4 <-c5 <-c6
                   ^         ^
                   pilot0.1  pilot0.2

At this point you can do a couple things to see how your code has changed.
For instance if you run `git diff pilot0.1 pilot0.2` git will spit out all the changes made to your files between tags `pilot0.1` and `pilot0.2`.
If you want to see just the changes to a specific file you can run `git diff pilot0.1 pilot0.2 expt.py`.
These commands work equally well if you use the commit checksums but tags let you use these more user friendly and semantically meaningful names.

This functionality is built into any git system so you can apply this feature when using a GUI system like sourcetree or the github app.

### ''Moving'' a tag

Now lets say you tried to run pilot 2 but realized there is a bug in your code.
You make a bug fix commit and now your tag is associated with a commit that isn't what was part of pilot 2.
You could add another tag (e.g. `pilot0.2-fixed`) or you can delete your previous tag and remake it on the new commit.
We go from this:

    c1 <-c2 <-c3 <-c4 <-c5 <-c6 <-c7
                   ^         ^
                   pilot0.1  pilot0.2

With the commands:

    git tag -d pilot0.2
    git tag -a pilot0.2 -m 'second pilot version'

To get this:

    c1 <-c2 <-c3 <-c4 <-c5 <-c6 <-c7
                   ^              ^
                   pilot0.1       pilot0.2

### Returning to a previous state using tags

You can return your current directory to a previous state using tags.
For example suppose you want to run your `pilot0.1` code to check for something.
You can return all the files that you are tracking to what they were like at `pilot0.1` with `git checkout pilot0.1`.
Looking through the directory you can find that all the files that are being tracked have returned to a previous version while any other files remain the same.
To return to where you were before run `git checkout master`.

A couple of notes before moving forward. When you run `git checkout` on a tag you will get a message telling you are in the ominous sounding `detached HEAD` state.
We will defer explaining what this means till later.
For now just know that you should not make any changes to your files in a `detached HEAD` state because they will be lost (yes even if you commit the changes).
Also you are probably wondering why `git checkout pilot0.2` isn't the way to return to where you where, this again is because doing so will keep you in a detached state.
This is probably one of the most confusing parts about git and tags specifically.

### Some details about tags

Tags are not shared between clones of a repository by default.
If you want to share tags between repositories you must enable the `--tags` argument like so:
    
    git push --tags

## Using branches to experiment

Branches are a core concept in VCSs but their representation and usage in git can be quite different than how other VCSs use them.
If you are using git you have already been using git branches without really knowing it.
When you `git init` in a directory, git automatically creates a branch called `master`.
We can then think of all commits made in that repository as being on the `master` branch:

    branch:                               master
                                          v
            c1 <-c2 <-c3 <-c4 <-c5 <-c6 <-c7
                           ^              ^
    tags:                  pilot0.1       pilot0.2

While you can think of a branch as a series of grouped commits this is not exactly the way they are represented by git.
Rather, git represents a branch similar to how it represents a tag.
A branch is a pointer to a commit. Branch pointers differ in that they move with commits.
Put another way, a branch represents where changes/developments happen where as a tag represents a point in history.

Just like tags and checksums you can use a branch name to reference a commit. In our example repository these commands are equivalent:

    git diff pilot0.1 pilot0.2
    git diff pilot0.1 master

And just like tags you can `git checkout` a branch to make the current directory update to the branch.
Unlike a tag, if you `git checkout` a branch and commit to it, the branch pointer moves to the new commit.

### two branches forward

We ran our two pilots and we realize that what makes the most sense moving forward is to run two separate experiments based on pilot 2.
We can do this by creating two branches off of our `master` branch, one for each experiment.

To set ourselves up, we first get back to the right place in history and create our branches by running:

    git checkout master        # brings us to where we want to branch
    git branch exprt-2         # this makes a branch
    git checkout -b exprt-1    # this makes and checks out a branch

And we now have set git up so that our next commit will be made to the `exprt-1` branch.
We make a few commits and finish up creating the experiment.
We then `git checkout exprt-1` and modify the code from `pilot0.2` into our second experiment.
This leaves us with a state like this:

              master
              v
    ...<-c6 <-c7
              ^
              |    exprt-1
              |    v
              |--- c8
              |
              |         exprt-2
              |         v
              |--- c9 <-c10

So we have our two experiments branching off our second pilot.
This lets us keep all the code and stimuli for the two experiments separate without having to copy files around or create new directories.
Switching between the two only takes a `git checkout`.

### one branch forward, one merge back

We run both experiments, tag our repository so we can be sure to get back to the right points in history, and add in analysis scripts for our data.
Our repository looks something like (branches point down to a commit, tags point up):

         master
         v
    ...<-c7
         ^                   exprt-1
         |                   v
         |--- c8 <-c12<-c13<-c14
         |    ^         ^
         |    pilot1.1  exprt.1
         |                   exprt-2
         |                   v
         |--- c9 <-c10<-c11<-c15
                   ^         ^
                   pilot2.1  exprt.2

We then realize what we really want to do is run a follow-up to the first experiment and a combined experiment 1 and 2.
We already know how to do the first part; we create a branch at `exprt-1` called `exprt-1b` and working from there:

    git checkout -b exprt-1b exprt-1
    ... #more progress
    ...
    git commit -m '...'

The second part also starts with a new branch but will require a `git merge` to join up experiments 1 and 2.

    git checkout -b exprt-1-2 exprt-2
    git merge exprt-1
    ... #handle the merge
    ...
    git commit -m 'Experiment 1-2...'

In the end we might get something like:

         master
         v
    ...<-c7
         ^                   exprt-1   exprt-1b
         |                   v         v
         |--- c8 <-c12<-c13<-c14<-c16<-c17
         |    ^         ^      ^       ^       
         |    pilot1.1  exprt.1 \      pilot1b.1
         |                       \
         |                        \      
         |                         \
         |                          \    
         |                           |   
         |                           |    exprt-1-2
         |                           |    v
         |                           ,--- c18
         |                   exprt-2/
         |                   v     /
         |--- c9 <-c10<-c11<-c15<-'
                   ^         ^
                   pilot2.1  exprt.2

Looks complicated but this is often how projects sprawl out. Navigating it only takes looking through your tags and branches and checking out the appropriate areas.
By keeping separate parts of it in branches we can swap into and out of experiments without worrying about overwriting work elsewhere.

### Some notes about branches (and tags)

Branches are everywhere in git. This becomes especially clear when you look into the mechanics of how git works.
For example, when you `git pull` from a remote repository what git does under the hood is retrieve the changes from the remote, puts them into a separate branch, and merges that branch into your local branch.
You can recreate what `git pull` does by using `git fetch origin` and then `git merge origin/master` to merge any changes on the `master` branch in the `origin` remote repository into which ever branch you currently are on.
When you clone a remote repository git will automatically place all the remote branches into the `origin` namespace and hence why remote changes to the `master` branch are in `origin/master` and local changes are in `master`.

Just like tags you can delete a branch.
Just like tags you can create a branch at any past commit by referencing that commit's checksum, tag, or branch name (e.g. `git checkout -b expt-1c expt-1`).
Similar to a tag you can annotate a branch using `git branch --edit-description <branch>`.
Just like commits made on top of a checked out tag, if you delete a branch and there are no other branches or tags that reference the commits on that branch git will mark it for deletion (this can be multiple commits!).

## Branch and tag lab-notebook best practices

### Branches =/= tags

While branches seem like beefed up tags they do have separate uses.
In this workflow tags are for commits that you will need to find later but do not want to change.
This can have specific uses in experimental/academic work, for example:

* Finding the exact stimuli you used for a pilot or experiment (e.g. to share with others)
* Finding the exact script you used to analyze data for a publication. 
* Get all the files you need to replicate a pilot or experiment (e.g. to copy them to a new experimental computer).
* Annotating commits that work/don't work (i.e. if you commit code that doesn't necessarily ''run'' or write-ups that are works in progress).
* References commits that you might return to for follow-ups
    * In our example we stopped working on the `exprt-1` and `exprt-2` branches so we can tag those commits and delete the branches. Since the tags reference them they will not be deleted.

### What to keep

#### Human readable files

Git and other VCSs track changes to repositories by looking for line-by-line differences in files.
This makes it useful for us, especially when merging files, since git can find conflicts and present them to us in a readable format.
Compiled files like executable programs, Word docs (.doc and .docx), Excel files (.xls, .xlsx), PDFs, graphs, etc. are typically encoded in binary format that does not have the same line-like structure of software code or stimuli files and certainly doesn't contain human readable information.
Git and other VCSs can track changes to binary files without a problem.
However, when it comes to giving us helpful information during merges these systems will be, out of the box, not helpful.
You can set up git to behave in a reasonable way with binary files and specifically help to pick between alternative versions of binary files (look [here](http://git-scm.com/book/en/v2/Customizing-Git-Git-Attributes) and [here](https://git-annex.branchable.com/)).

A good rule of thumb is to add files to a repository if you cannot automatically generate it using the files in the repository, otherwise leave them out.
For example, if you have a repository with data and analysis scripts that generate figures it is better to keep just the data and scripts than the figures themselves.
This also helps with keeping repositories a reasonable size, VCSs are good at compressing text file changes but binary file changes are harder to compress in the same way.

#### IRB safe history

Keeping a project in a repository is a great way to keep track of progress and offers a good way to experiment with designs and software analyses.
Because what you commit is saved into a recoverable format it's important to keep track of whether some things _should_ be saved.
The clearest issue is in saving experimental subject data.
It is best that you never commit identifiable subject data into a repository, either by never keeping the subject data in the same place as the repository or making sure your data is de-identified.
A commit will not hide this data from anyone you share the repository with.

One way to avoid accidently saving human subjects data is to put all data into a specific directory and have git ignore it by specifying it in a `.gitignore` [file](https://help.github.com/articles/ignoring-files/).
This will hide these files when you run `git status` and will also ignore them when using `git add --all`.
The only way you can then put ignored files into a repository is by explicitly specifying them using `git add <file>`.
